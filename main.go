package main

import (
	"log"
	"context"
	"encoding/json"
	"net/http"

	"time"

	"github.com/go-redis/redis/v8"
)

// UserType merupakan type data Users
type UserType struct{
	Name string `json:"name"`
	Address string `json:"address"`
}

var (
	client *redis.Client
)

func main() {
	http.HandleFunc("/", getUserHandler)
	log.Println("Server running on port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func getUserHandler(w http.ResponseWriter, r *http.Request){
	var user [] UserType
	ctx := context.Background()
	
	if client == nil {
		client = newRedisClient()
	}

	// check apakah data dengan key "my-key" ada di cache
	keyExist, err := client.Exists(ctx, "my-key").Result()

	if err != nil {
		panic(err)
	}

	// jika data tidak ada di cache
	if keyExist == 0 {

		// ambil dari database
		user = getUserFromDatabase()
		
		// konversikan ke format JSON
		p, err := json.Marshal(user)
		
		if err != nil {
			panic(err)
		}
		
		// simpan kedalam cache dengan menggunakan key "my-key" dan disimpan selama 5 menit
		_, _ = client.Set(ctx, "my-key", p, 5 * time.Minute).Result()

	}else{ // jika data ada di cache
	
		// ambil dari cache dengan key "my-key"
		resultCache, err := client.Get(ctx, "my-key").Result()
		if err != nil {
			panic(err)
		}

		// konversikan dari string JSON ke object
		_ = json.Unmarshal([]byte(resultCache), &user)
	}

	// tampilkan sebagai result json ke client
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

// newRedisClient adalah fungsi untuk membuat koneksi ke redis
func newRedisClient() *redis.Client{
	client := redis.NewClient(&redis.Options{
		Addr:     "redis-server:6379",
		Password: "", // no password set
		DB:       0,
	})

	return client
}

// getUserFromDatabase adalah fungsi ambil data seolah-olah dari database
func getUserFromDatabase() [] UserType{
	users := []UserType{
		UserType{
			Name: "John Doe",
			Address: "Bandung",
		},
		UserType{
			Name: "Brad Pitt",
			Address: "Jakarta",
		},
	}

	return users
}