module simple-rest-cache

go 1.13

require (
	github.com/Azure/azure-sdk-for-go v42.3.0+incompatible // indirect
	github.com/Azure/go-autorest/autorest v0.10.2 // indirect
	github.com/Azure/go-autorest/autorest/to v0.3.0 // indirect
	github.com/Azure/go-autorest/autorest/validation v0.2.0 // indirect
	github.com/go-redis/redis v6.15.8+incompatible // indirect
	github.com/go-redis/redis/v8 v8.0.0-beta.2
)
