FROM golang

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
COPY ./ ./

RUN go mod download
RUN go build -o simple-rest-cache .

CMD ["./simple-rest-cache"]